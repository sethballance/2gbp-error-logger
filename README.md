# Getting Started
```
pipenv install
pipenv run python 2gbp.py
```
This will default to use the pi camera and will switch to a webcam if an error
is thrown when starting the pi camera. If you want it to default to the webcam
then run one of the following commands instead:
```
pipenv run python 2gbp.py --camera=1
pipenv run python 2gbp.py -c=1
```
The program will initialize everything necessary and then the video feed will
show. At this time you may adjust the camera so that the syringe is positioned
in the center of the image and in focus. Press 'space' to continue as prompted.
The program will then sit and do nothing while you prepare the print process
and run the purge cycle. Press 's' after the purge cycle is complete and the
print process is started. Run until complete and press 'q' to end recording.
Be sure to end the recording as soon as the print is complete or just before so
that there aren't a bunch of files logged as errors at the end.

# Training Model Updates
In order to refresh the model just delete the folder '2gbp_model'. The next time
the program is run, it will automatically create a new model. You will need to
do this any time that the images in `trainer\trainingImages` are changed or any
other images are added their proper directories.

[View original repository](https://gitlab.com/sethballance/2gbp-error-logger)