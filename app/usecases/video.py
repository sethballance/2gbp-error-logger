import cv2
import datetime
import constants
from app.usecases.webcamvideo import WebcamVideo

class VideoUseCase(object):
    def __init__(self, cam=0):
        resolution = constants.RESOLUTION
        if cam == 0:
            from app.usecases.pivideo import PiVideo
            self.stream = PiVideo(resolution)
        else:
            self.stream = WebcamVideo(resolution)
        
    def start(self):
        return self.stream.start()

    def update(self):
        return self.stream.update()

    def read(self):
        # convert to gray and then back to color so that it can have color text on gray image
        frame = self.stream.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)

        # draw the timestamp on the frame
        timestamp = datetime.datetime.now()
        ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
        cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
            0.35, (0, 0, 255), 1)

        return frame

    def stop(self):
        self.stream.stop()
        cv2.destroyAllWindows()
