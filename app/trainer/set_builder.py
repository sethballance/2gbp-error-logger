import os
import constants
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.preprocessing.image import ImageDataGenerator

DATADIR = os.path.join(os.getcwd(), constants.TRAINING_IMAGES_DIR)
MODEL_NAME = '2gbp_model'

class Trainer(object):
    def __init__(self):
        self.training_set = []
        self.model = None

    def create_training_set(self):
        try:
            self.model = load_model(MODEL_NAME)
        except:
            self.__train()
    
    def __train(self):
        # The 1./255 is to convert from uint8 to float32 in range [0,1].
        image_generator = ImageDataGenerator(rescale=1./255)

        self.training_set = image_generator.flow_from_directory(directory=DATADIR,
            batch_size=constants.BATCH_SIZE,
            shuffle=True,
            color_mode='grayscale',
            target_size=(constants.IMG_HEIGHT, constants.IMG_WIDTH),
            class_mode='categorical'
        )

        self.model = Sequential([
            Flatten(input_shape=(constants.IMG_HEIGHT, constants.IMG_WIDTH)),
            Dense(128, activation='relu'),
            Dense(2) # number of categories
        ])

        self.model.compile(optimizer='adam',
              loss=CategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])
        
        self.model.summary()

        self.model.fit(self.training_set, batch_size=constants.BATCH_SIZE, epochs=10)

        self.model.save(MODEL_NAME)