import constants
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Softmax

class Tester(object):
    def __init__(self, model):
        self.model = model
        self.classifier = Sequential([model, Softmax()])

    def predict(self, frame):
        frame = frame.reshape(-1, constants.IMG_HEIGHT, constants.IMG_WIDTH, 1)
        frame = frame/255.0
        prediction = self.classifier.predict(frame)
        classification = self.classify(prediction[0])
        return classification

    def classify(self, prediction):
        if prediction[0] < .05 and prediction[1] < .05 : # not working quite as expected
            print("It appears that you are not filming 2GBP. If you are, you may need to update the model by deleting the current one and starting the process over.")
            raise Exception('Image does not match trained model.')
        if prediction[0] > .8: #bad
            return 0
        elif prediction[0] > .3 or prediction[1] < .8: #questionable
            return 1
        else: #good
            return 2