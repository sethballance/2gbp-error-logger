from datetime import datetime
from app.handler.logger import Logger

class Handler(object):
    def __init__(self):
        self.logger = Logger()
    def handle(self, status, frame):
        if status == 0:
            self.log(self.getTimeStamp(), frame)
        elif status == 1:
            self.log(self.getTimeStamp() + '_questionable', frame)

    def getTimeStamp(self):
        time = datetime.now()
        return time.strftime("%Y_%m_%d_%H%M%S%f")

    def log(self, filename, frame):
        self.logger.log(filename, frame)
