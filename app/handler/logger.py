import os
from PIL import Image

LOGS_DIR = os.path.join(os.getcwd(), "app\\logs")

class Logger(object):
    def __init__(self):
        try:
            os.makedirs(LOGS_DIR)
        except OSError:
            pass

    def log(self, filename, image):
        im = Image.fromarray(image)
        save_as = os.path.join(LOGS_DIR, filename + ".jpg")
        im.save(save_as)
