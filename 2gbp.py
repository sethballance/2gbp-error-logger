from runner import Runner

# initializes training and camera settings
runner = Runner()

# Period of time to position camera. Press space to continue
runner.calibrate_camera()

# starts recording
runner.run_camera()