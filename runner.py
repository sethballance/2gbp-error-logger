import cv2
import argparse
import constants
from app.trainer.set_builder import Trainer
from app.usecases.video import VideoUseCase
from app.tester.classification import Tester
from app.handler.handler import Handler

class Runner(object):
	def __init__(self):
		self.camera = None
		self.current_frame = None
		self.tester = None
		self.initialize_training()
		self.initialize_camera()
		self.handler = Handler()

	def initialize_camera(self):
		# construct the argument parse and parse the arguments
		ap = argparse.ArgumentParser()
		ap.add_argument("-c", "--camera", type=int, default=0,
			help="Which camera should be used. PiCamera=0, WebCam=1")
		args = vars(ap.parse_args())

		try:
			self.camera = VideoUseCase(args["camera"])
		except:
			self.camera = VideoUseCase(1)

		# Start self.camera
		self.camera.start()

	def initialize_training(self):
		# Fetch training images and train
		trainer = Trainer()
		trainer.create_training_set()
		self.tester = Tester(trainer.model)

	def calibrate_camera(self):
		print('Press the space-bar when the camera is positioned properly.')
		while True:
			# grab the frame from the threaded video stream
			self.current_frame = self.camera.read()

			# show the frame
			cv2.imshow("Frame", self.current_frame)

			key = cv2.waitKey(1) & 0xFF

			# if the space-bar was pressed, break from the loop
			if key == ord(' '):
				break


	def run_camera(self):
		print('Press "s" when the print process has started.')

		while True:
			key = cv2.waitKey(1) & 0xFF

			# if 's' was pressed, start recording.
			if key == ord('s'):
				print('starting recording')
				break

		print('Press "q" when the print is complete.')
		while True:
			# grab the frame from the threaded video stream
			self.current_frame = self.camera.read()

			# show the frame
			cv2.imshow("Frame", self.current_frame)

			self.current_frame = cv2.resize(self.current_frame,
				(constants.IMG_WIDTH, constants.IMG_HEIGHT))

			self.check_frame()

			key = cv2.waitKey(1) & 0xFF

			# if the `q` key was pressed, break from the loop
			if key == ord("q"):
				break

		# do a bit of cleanup
		self.camera.stop()

	def check_frame(self):
		status = self.tester.predict(self.current_frame)
		self.handler.handle(status, self.current_frame)
